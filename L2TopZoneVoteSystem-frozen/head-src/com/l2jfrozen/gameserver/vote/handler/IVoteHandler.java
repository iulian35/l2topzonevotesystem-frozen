package com.l2jfrozen.gameserver.vote.handler;

/**
 * @author L2Cygnus
 */
public interface IVoteHandler {
	boolean isVoted(String ip);
	int getTotalVotes();
}
