package com.l2jfrozen.gameserver.vote.handler;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ScheduledFuture;

import com.l2jfrozen.Config;
import com.l2jfrozen.gameserver.datatables.SkillTable;
import com.l2jfrozen.gameserver.datatables.sql.ItemTable;
import com.l2jfrozen.gameserver.model.L2Skill;
import com.l2jfrozen.gameserver.model.L2World;
import com.l2jfrozen.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfrozen.gameserver.model.entity.Announcements;
import com.l2jfrozen.gameserver.network.L2GameClient;
import com.l2jfrozen.gameserver.network.serverpackets.CreatureSay;
import com.l2jfrozen.gameserver.network.serverpackets.ItemList;
import com.l2jfrozen.gameserver.templates.L2Item;
import com.l2jfrozen.gameserver.thread.ThreadPoolManager;
import com.l2jfrozen.gameserver.vote.AbstractVoteManager;
import com.l2jfrozen.gameserver.vote.sql.VotersTable;

/**
 * @author L2Cygnus
 */
public class VoteHandler extends AbstractVoteManager {

    private Map<String, Long> _voterMap;
    private VotersTable _votersTable;
    private IVoteHandler _voteHandler;
    private ScheduledFuture<?> _votersCleanTask;
    private ScheduledFuture<?> _globalVotesCheckTask;
    private ScheduledFuture<?> _voteEncourageTask;
    private int _lastRewardVotes;

    public VoteHandler() {
        loadVoters();
        cleanVoters();
        _lastRewardVotes = getVoteHandler().getTotalVotes();
        stopAutoTasks();
        _voteEncourageTask = ThreadPoolManager.getInstance().scheduleGeneralAtFixedRate(new AutoVoteEncourageTask(), Config.VOTE_ENCOURAGE_INIT_TIME, Config.VOTE_ENCOURAGE_DELAY_TIME);
        _votersCleanTask = ThreadPoolManager.getInstance().scheduleGeneralAtFixedRate(new AutoVotersCleanTask(), Config.VOTE_VOTERS_CLEAN_INIT_TIME, Config.VOTE_VOTERS_CLEAN_DELAY_TIME);
        if (Config.VOTE_ENABLE_GLOBAL_REWARD_TASK) {
            _globalVotesCheckTask = ThreadPoolManager.getInstance().scheduleGeneralAtFixedRate(new AutoGlobalVotesRewardTask(), Config.VOTE_GLOBAL_REWARD_INIT_TIME, Config.VOTE_GLOBAL_REWARD_DELAY_TIME);
        }
    }

    public void stopAutoTasks() {
        if (_votersCleanTask != null) {
            _votersCleanTask.cancel(true);
            _votersCleanTask = null;
        }
        if (_globalVotesCheckTask != null) {
            _globalVotesCheckTask.cancel(true);
            _globalVotesCheckTask = null;
        }
        if (_voteEncourageTask != null) {
            _voteEncourageTask.cancel(true);
            _voteEncourageTask = null;
        }
    }

    /**
     * Load voters from DB
     */
    private void loadVoters() {
        getVoterMap().putAll(getVotersTable().loadVoters());
    }

    private boolean isAlreadyVoted(L2PcInstance voter, String ip, boolean sendTimeLeftMsg, boolean sendExpireSoonMsg) {

        Long voteTime = getVoterMap().get(ip);
        if (voteTime == null) {
            return false;
        }
        if (sendTimeLeftMsg) {
            voter.sendMessage(getTimeLeftMsg(voteTime + Config.VOTE_EXPIRE_TIME));
        }
        if (sendExpireSoonMsg && isVoteExpireSoon(voteTime + Config.VOTE_EXPIRE_TIME)) {
            sendEncourageToVoteMsg(voter, Config.VOTE_EXPIRATION_NOTIFY_MESSAGE);
        }
        return true;
    }

    /**
     * @param voteTime
     * @return true if vote time in milliseconds + vote expire time is less than current time
     */
    private boolean isTimeExpired(long voteTime) {
        if ((voteTime + Config.VOTE_EXPIRE_TIME) < System.currentTimeMillis()) {
            return true;
        }
        return false;
    }

    private String getTimeLeftMsg(long timeLeft) {
        long diff = timeLeft - System.currentTimeMillis();
        if (diff <= 0) {
            return "You will be able to vote again in few minutes";
        }
        int minutes = (int) ((diff / (1000 * 60)) % 60);
        int hours = (int) ((diff / (1000 * 60 * 60)) % 24);
        return String.format("You will be able to vote again in %s hours and %s min", hours, minutes);
    }

    public void handleVote(L2PcInstance voter) {

        String ip = tryGetIp(voter);
        if (ip.isEmpty()) {
            return;
        }

        if (isAlreadyVoted(voter, ip, true, false)) {
            return;
        }
        IVoteHandler handler = getVoteHandler();
        if (handler.isVoted(ip)) {
            if (Config.VOTE_REWARD_ITEM) {
                if (!isItemValid(Config.VOTE_REWARD_ITEM_ID, Config.VOTE_REWARD_ITEM_COUNT)) {
                    return;
                }
                voter.getInventory().addItem("L2TopZone reward", Config.VOTE_REWARD_ITEM_ID, Config.VOTE_REWARD_ITEM_COUNT, voter, null);
                voter.sendPacket(new ItemList(voter, true));
            } else {
                L2Skill skill = SkillTable.getInstance().getInfo(Config.VOTE_REWARD_SKILL_ID, Config.VOTE_REWARD_SKILL_LVL);
                if (skill == null) {
                    _log.warning(String.format("Failed to create skill, skill with %s id and %s level doesn't exists", Config.VOTE_REWARD_SKILL_ID, Config.VOTE_REWARD_SKILL_LVL));
                    return;
                }
                voter.addSkill(skill, false);
                voter.sendSkillList();
            }
            getVoterMap().put(ip, System.currentTimeMillis());
            getVotersTable().saveVoter(ip);
            voter.sendMessage("Thanks for your vote");
        } else {
            voter.sendMessage("You didn't voted.");
        }
    }

    /**
     * @param voter
     * @return empty String if client is null or detached else IP
     */
    private String tryGetIp(L2PcInstance voter) {
        L2GameClient client = voter.getClient();
        if (client == null || client.isDetached())
            return "";

        return client.getConnection().getInetAddress().getHostAddress();
    }

    private Map<String, Long> getVoterMap() {
        if (_voterMap == null) {
            _voterMap = new HashMap<>();
        }
        return _voterMap;
    }

    private IVoteHandler getVoteHandler() {
        if (_voteHandler == null) {
            _voteHandler = new TopZoneVoteHandler();
        }
        return _voteHandler;
    }

    private VotersTable getVotersTable() {
        if (_votersTable == null) {
            _votersTable = new VotersTable();
        }
        return _votersTable;
    }

    /**
     * Reward skill are not saved to characters_skills table,
     * so every time when player logs in game or change sub class reward skill has to be added again
     *
     * @param voter
     */
    private void giveRewardSkill(L2PcInstance voter) {
        if (Config.VOTE_REWARD_ITEM) {
            return;
        }
        String ip = tryGetIp(voter);
        if (ip.isEmpty()) {
            return;
        }
        Long voteTime = getVoterMap().get(ip);
        if (voteTime == null || isTimeExpired(voteTime)) {
            return;
        }
        L2Skill skill = SkillTable.getInstance().getInfo(Config.VOTE_REWARD_SKILL_ID, Config.VOTE_REWARD_SKILL_LVL);
        if (skill == null) {
            _log.warning(String.format("Failed to create skill, skill with %s id and %s level doesn't exists", Config.VOTE_REWARD_SKILL_ID, Config.VOTE_REWARD_SKILL_LVL));
            return;
        }
        voter.addSkill(skill, false);
        voter.sendSkillList();
    }

    public void cleanVoters() {
        Set<String> removeIp = new HashSet<>();
        Iterator<Entry<String, Long>> entries = getVoterMap().entrySet().iterator();
        while (entries.hasNext()) {
            Map.Entry<String, Long> votersMap = entries.next();
            if (isTimeExpired(votersMap.getValue())) {
                removeIp.add(votersMap.getKey());
                entries.remove();
            }
        }
        if (!Config.VOTE_REWARD_ITEM) {
            L2Skill skill = SkillTable.getInstance().getInfo(Config.VOTE_REWARD_SKILL_ID, Config.VOTE_REWARD_SKILL_LVL);
            if (skill != null) {
                for (L2PcInstance p : L2World.getInstance().getAllPlayers()) {
                    String ip = tryGetIp(p);
                    if (!ip.isEmpty() && removeIp.contains(ip)) {
                        p.removeSkill(skill, false);
                        p.sendSkillList();
                    }
                }
            }
        }
        if (!removeIp.isEmpty()) {
            getVotersTable().deleteVoters(removeIp);
        }
    }

    private boolean isItemValid(int id, int count) {
        L2Item itemTemplate = ItemTable.getInstance().getTemplate(id);
        if (itemTemplate == null) {
            _log.warning(String.format("Failed to create item, item with %s id doesn't exists", id));
            return false;
        }
        if (count > 1 && !itemTemplate.isStackable()) {
            _log.warning(String.format("Failed to create item, item %s is not stackable", itemTemplate.getName()));
            return false;
        }
        return true;
    }

    private boolean isVoteExpireSoon(long timeLeft) {
        if (timeLeft - Config.VOTE_MINS_LEFT_TO_NOTIFY_VOTER < System.currentTimeMillis()) {
            return true;
        }
        return false;
    }

    private void sendEncourageToVoteMsg(L2PcInstance activeChar, String msg) {
        activeChar.sendPacket(new CreatureSay(activeChar.getObjectId(), 2, "L2TopZone", msg));
    }

    private void checkGlobalVotes() {
        // failed to read votes on vote handler load, try to read votes again
        if (_lastRewardVotes == -1) {
            _lastRewardVotes = getVoteHandler().getTotalVotes();
            return;
        }
        int currentVotes = getVoteHandler().getTotalVotes();
        // failed to read votes abort further actions
        if (currentVotes == -1) {
            return;
        }
        if (_lastRewardVotes + Config.VOTE_GLOBAL_REWARD_VOTE_COUNT <= currentVotes) {
            _lastRewardVotes = currentVotes;
            if (!isItemValid(Config.VOTE_GLOBAL_REWARD_ITEM_ID, Config.VOTE_GLOBAL_REWARD_ITEM_COUNT)) {
                return;
            }
            Set<String> rewardedIp = new HashSet<>();
            for (L2PcInstance p : L2World.getInstance().getAllPlayers()) {
                String ip = tryGetIp(p);
                if ((!Config.VOTE_GLOBAL_REWARD_DUALBOX && rewardedIp.contains(ip)) || ip.isEmpty()) {
                    continue;
                }
                rewardedIp.add(ip);
                p.getInventory().addItem("L2TopZone reward", Config.VOTE_GLOBAL_REWARD_ITEM_ID, Config.VOTE_GLOBAL_REWARD_ITEM_COUNT, p, null);
            }
            Announcements.getInstance().announceToAll("[AutoVoteReward] Great Work! Check your inventory for Reward!!");
        } else {
            Announcements.getInstance().announceToAll(String.format("[AutoVoteReward] Current TOPZONE Votes %d ", currentVotes));
            Announcements.getInstance().announceToAll(String.format("[AutoVoteReward] Next TOPZONE Reward at %d Votes!!", (_lastRewardVotes + Config.VOTE_GLOBAL_REWARD_VOTE_COUNT)));
        }
    }

    private static class SingletonHolder {
        protected static final VoteHandler _instance = new VoteHandler();
    }

    public static VoteHandler getInstance() {
        return VoteHandler.SingletonHolder._instance;
    }

    protected class AutoVotersCleanTask implements Runnable {

        @Override
        public void run() {
            _log.info("AutoVotersCleanTask: voters clean task called");
            cleanVoters();
        }
    }

    protected class AutoGlobalVotesRewardTask implements Runnable {

        @Override
        public void run() {
            _log.info("AutoGlobalVotesRewardTask: global vote reward task called");
            checkGlobalVotes();
        }
    }

    protected class AutoVoteEncourageTask implements Runnable {

        @Override
        public void run() {
            _log.info("AutoVoteEncourageTask: vote encourage task called");
            for (L2PcInstance p : L2World.getInstance().getAllPlayers()) {
                String ip = tryGetIp(p);
                if (ip.isEmpty()) {
                    continue;
                }
                if (!isAlreadyVoted(p, ip, false, true)) {
                    sendEncourageToVoteMsg(p, Config.VOTE_ENCOURAGE_MESSAGE);
                }
            }
        }
    }
}
