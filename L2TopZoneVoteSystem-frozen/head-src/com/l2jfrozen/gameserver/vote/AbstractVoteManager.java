package com.l2jfrozen.gameserver.vote;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.logging.Logger;
import com.l2jfrozen.Config;

/**
 * @author L2Cygnus
 */
public abstract class AbstractVoteManager {

	protected static final Logger _log = Logger.getLogger(AbstractVoteManager.class.getName());

	public String getResponse(String url, String userAgent) {
		String response = "";
		try {
			final URL obj = new URL(url);
			final HttpURLConnection con = (HttpURLConnection) obj.openConnection();

			// add request header
			con.setConnectTimeout(1000);
			con.addRequestProperty("User-Agent", userAgent);

			final int responseCode = con.getResponseCode();
			if (responseCode == 200) // OK
			{
				final StringBuilder sb = new StringBuilder();
				try (BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()))) {
					String inputLine;
					while ((inputLine = in.readLine()) != null) {
						sb.append(inputLine);
					}
				}
				response = sb.toString();
			}
		} catch (Exception e) {
			if (Config.VOTE_LOG_ERROR) {
				e.printStackTrace();
			}
		}
		return response;
	}
}
