package com.l2jfrozen.gameserver.vote.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.l2jfrozen.Config;
import com.l2jfrozen.util.database.L2DatabaseFactory;

/**
 * @author L2Cygnus
 */
public class VotersTable {

    public void saveVoter(String ip) {

        try (Connection con = L2DatabaseFactory.getInstance().getConnection()) {
            final PreparedStatement stmt = con.prepareStatement("INSERT INTO topzone_voters (ip,voted_time) values(?,?)");
            stmt.setString(1, ip);
            stmt.setLong(2, System.currentTimeMillis());
            stmt.executeUpdate();
            stmt.close();
            con.close();
        } catch (SQLException e) {
            if (Config.VOTE_LOG_ERROR) {
                e.printStackTrace();
            }
        }
    }

    public Map<String, Long> loadVoters() {
        Map<String, Long> voterMap = new HashMap<>();
        try (Connection con = L2DatabaseFactory.getInstance().getConnection()) {
            final PreparedStatement stmt = con.prepareStatement("SELECT ip, voted_time FROM topzone_voters");
            final ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                voterMap.put(rs.getString("ip"), rs.getLong("voted_time"));
            }
            rs.close();
            stmt.close();
            con.close();
        } catch (SQLException e) {
            if (Config.VOTE_LOG_ERROR) {
                e.printStackTrace();
            }
        }
        return voterMap;
    }

    public void deleteVoter(String ip) {
        try (Connection con = L2DatabaseFactory.getInstance().getConnection()) {
            PreparedStatement prep = con.prepareStatement("DELETE FROM topzone_voters WHERE ip = ?");
            prep.setString(1, ip);
            prep.executeUpdate();
            prep.close();
            con.close();
        } catch (SQLException e) {
            if (Config.VOTE_LOG_ERROR) {
                e.printStackTrace();
            }
        }
    }

    public void deleteVoters(Set<String> voters) {
        try (Connection con = L2DatabaseFactory.getInstance().getConnection()) {
            PreparedStatement prep = con.prepareStatement("DELETE FROM topzone_voters WHERE ip = ?");
            for (String ip : voters) {
                prep.setString(1, ip);
                prep.executeUpdate();
            }
            prep.close();
            con.close();
        } catch (SQLException e) {
            if (Config.VOTE_LOG_ERROR) {
                e.printStackTrace();
            }
        }
    }
}